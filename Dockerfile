FROM alpine:latest
RUN apk update && apk upgrade
RUN apk add nginx php81 php81-fpm php81-mbstring php81-pdo php81-opcache php81-gd php81-zlib php81-curl php81-xml php81-pdo_sqlite sqlite

RUN sed -i 's/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm.sock/g' /etc/php81/php-fpm.d/www.conf
RUN sed -i 's/;listen.owner = nobody/listen.owner = nginx/g' /etc/php81/php-fpm.d/www.conf
RUN sed -i 's/;listen.group = nobody/;listen.group = nginx/g' /etc/php81/php-fpm.d/www.conf
RUN rm -rf /etc/nginx/http.d/default.conf

COPY container/cutekb.conf /etc/nginx/http.d/
RUN mkdir -pv /var/www/cutekb
COPY src/ /var/www/cutekb/
RUN chown -R nginx:nginx /var/www/cutekb
RUN chmod -R 755 /var/www/cutekb
RUN chmod -R 777 /var/www/cutekb/db
EXPOSE 80

VOLUME /var/www/cutekb/db
VOLUME /var/log/nginx

STOPSIGNAL SIGTERM
CMD ["/bin/sh", "-c", "php-fpm81 && nginx -g 'daemon off;' -c /etc/nginx/nginx.conf"]