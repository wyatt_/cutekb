$(document).ready(function () {
  var refresh = true;
  $("#search").keyup(function (event) {
    var value = $(this).val();
    if (value.length >= 3) {
      $('#sql_table').load("lib/php/action.php", {
        firstRefreshTable: refresh,
        val: value
      });
      $('#content').load("lib/php/action.php", {
        firstRefreshData: refresh,
        val: value
      });
      $('#scrollspy').load("lib/php/action.php", {
        firstRefreshToc: refresh,
        val: value
      });
    }
  });
  
  
  var easyMDE;
  if (typeof easyMDE == 'undefined' || easyMDE == null) {
    easyMDE = new EasyMDE({ element: document.getElementById("editorarea"), autofocus: true, autoRefresh: true });
  }

  $("#btn-del").click(function () {
    $("#delete").modal("show");
  });

  $("#btn-confirm").click(function () {
    var myId = $('#editorarea').attr('name');
    $.post("lib/php/action.php",
      {
        id: myId,
        delete: true
      }
    );
    $("#btn-refresh")[0].click();
  });

  $("#btn-refresh").click(function () {
    $('#btn-edit').addClass("disabled");
    $('#btn-del').addClass("disabled");
    $('#sql_table').load("lib/php/action.php", {
      firstRefreshTable: refresh
    });

    $('#content').load("lib/php/action.php", {
      firstRefreshData: refresh
    });

    $('#scrollspy').load("lib/php/action.php", {
      firstRefreshToc: refresh
    });
  });

  $("#btn-save").click(function () {
    var myId = $('#editorarea').attr('name');
    var myTopic = $('#topic').val();
    var myContnet = easyMDE.value();
    $.post("lib/php/action.php",
      {
        id: myId,
        topic: myTopic,
        content: myContnet
      }
    );
    $("#btn-refresh")[0].click();
  });

  $("#btn-new").click(function () {
    $('#editorarea').attr('name', 0)
    easyMDE.value(' ');
    $('#topic').val('');
    $("#editor").modal("show");
  });

  $("#btn-edit").click(function () {
    easyMDE.value($('#editorarea').val());
    $("#editor").modal("show");
  });

  $('#sql_table').load("lib/php/action.php", {
    firstRefreshTable: refresh
  });

  $('#content').load("lib/php/action.php", {
    firstRefreshData: refresh
  });

  $('#scrollspy').load("lib/php/action.php", {
    firstRefreshToc: refresh
  });

  $("#sql_table").on("click", "tr", function (event) {
    var myId = $(this).attr('id');
    var value = $("#search").val();
    $('#btn-edit').removeClass("disabled");
    $('#btn-del').removeClass("disabled");
    $("#sql_table tr").each(function () {
      if ($(this).attr('id') == myId) {
        $(this).addClass("table-primary");
        $('#editorarea').attr('name', myId);
        $('#topic').val($(this).find("td:first").html());
        $('#delete-content').attr('name', myId);
        $('#delete-content').text('Are you sure, that you\'d like to remove entry ' + $(this).find("td:first").html() + ' ?'); 
      } else {
        $(this).removeClass("table-primary");
      }
    });
    if (value.length >= 3) {
      $('#content').load("lib/php/action.php", {
        firstRefreshData: refresh,
        id: myId,
        val: value
      });
      $('#scrollspy').load("lib/php/action.php", {
        firstRefreshToc: refresh,
        id: myId,
        val: value
      });
    } else {
      $('#content').load("lib/php/action.php", {
        firstRefreshData: refresh,
        id: myId
      });
      $('#scrollspy').load("lib/php/action.php", {
        firstRefreshToc: refresh,
        id: myId
      });
      $('#editorarea').load("lib/php/action.php", {
        firstRefreshEditor: refresh,
        id: myId
      });
    }
  });
});