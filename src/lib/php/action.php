<?php
include_once('db.php');
include_once('parsedown.php');
include_once('parsedownextra.php');
include_once('parsedowntoc.php');

$query = 'SELECT * FROM Main';

if (isset($_POST['firstRefreshTable'])) {
  if (isset($_POST['val'])) {
    $query = 'SELECT * FROM Main WHERE contentData LIKE \'%' . $_POST['val'] . '%\'';
  } else {
    $query = 'SELECT * FROM Main';
  }
  $results = $db->query($query);
  $i = 1;
  foreach($results as $row) {
    if ($i == 1) {
      echo "<tr class=\"table-primary\" id=\"" . $row['idContent'] . "\">";
      $i = $i + 1;
    } else {
      echo "<tr id=\"" . $row['idContent'] . "\">";
    }
    echo "<td>" . $row['contentTopic'] . "</td>";
    echo "<td>" . $row['groupName'] . "</td>";
    echo "</tr>";
  }
}

if (isset($_POST['firstRefreshData'])) {
  if (isset($_POST['id'])) {
    $query = 'SELECT * FROM Main WHERE idContent=' . $_POST['id'];
  } elseif (isset($_POST['val'])) {
    $query = 'SELECT * FROM Main WHERE contentData LIKE \'%' . $_POST['val'] . '%\'';
  } else {
    $query = 'SELECT * FROM Main';
  }
  $results = $db->query($query);
  $i = 1;
  foreach ($results as $row) {
    if ($i == 1) {
      $content = $row['contentData'];
      $i = $i + 1;
    }
    $Parsedown = new \ParsedownToc();
    if (isset($_POST['val'])) {
      $str = $Parsedown->body($content);
      $pattern = '/' . $_POST['val'] . '/i' ;
      $replacement = '<mark>' . $_POST['val'] . '</mark>';
      echo preg_replace($pattern, $replacement, $str);
    } else {
      echo $Parsedown->body($content);
      break;
    }
  }
}

if (isset($_POST['firstRefreshToc'])) {
  if (isset($_POST['id'])) {
    $query = 'SELECT * FROM Main WHERE idContent=' . $_POST['id'];
  } elseif (isset($_POST['val'])) {
    $query = 'SELECT * FROM Main WHERE contentData LIKE \'%' . $_POST['val'] . '%\'';
  } else {
    $query = 'SELECT * FROM Main';
  }
  $results = $db->query($query);
  $i = 1;
  foreach ($results as $row) {
    if ($i == 1) {
      $content = $row['contentData'];
      $i = $i + 1;
    }
    $Parsedown = new \ParsedownToC();
    if (isset($_POST['val'])) {
      $Parsedown->body($content);
      $str = $Parsedown->contentsList();
      $pattern = '/' . $_POST['val'] . '/i' ;
      $replacement = '<mark>' . $_POST['val'] . '</mark>';
      echo preg_replace($pattern, $replacement, $str);
    } else {
      $Parsedown->body($content);
      echo $Parsedown->contentsList();
      break;
    }
  }
}

if (isset($_POST['firstRefreshEditor'])) {
  if (isset($_POST['id'])) {
    $query = 'SELECT * FROM Main WHERE idContent=' . $_POST['id'];
  } elseif (isset($_POST['val'])) {
    $query = 'SELECT * FROM Main WHERE contentData LIKE \'%' . $_POST['val'] . '%\'';
  } else {
    $query = 'SELECT * FROM Main';
  }
  $results = $db->query($query);
  $i = 1;
  foreach ($results as $row) {
    if ($i == 1) {
      $content = $row['contentData'];
      $i = $i + 1;
      echo $content;
    }
  }
}

if (isset($_POST['id']) && isset($_POST['topic']) && isset($_POST['content'])) {
  $result;
  $id = $_POST['id'];
  $topic = $_POST['topic'];
  $content = $_POST['content'];
  if ($id == 0) {
    $result = $db->prepare("INSERT INTO Content (contentData, contentTopic, groupId) VALUES(:contentData, :contentTopic, '2')");
    $result->bindParam(":contentData", $content);
    $result->bindParam(":contentTopic", $topic);
  } else {
    $result = $db->prepare("UPDATE Content SET contentData=:contentData, contentTopic=:contentTopic WHERE idContent=:idContent");
    $result->bindParam(":contentData", $content);
    $result->bindParam(":contentTopic", $topic);
    $result->bindParam(":idContent", $id);
  }
  $result->execute();
}

if (isset($_POST['id']) && isset($_POST['delete'])) {
  $id = $_POST['id'];
  if ($id != 0) {
    $result = $db->prepare("DELETE FROM Content WHERE idContent=:idContent");
    $result->bindParam(":idContent", $id);
    $result->execute();
  }
}
?>