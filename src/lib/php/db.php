<?php
  global $db;
  $filename = '../../db/cuteKB.sqlite';

  if (!file_exists($filename)) {
    $db = new PDO('sqlite:' . $filename);
    if (!$db) {
      echo "Error: Unable to connect to database.";
      exit();
    } else {
      $sql = "CREATE TABLE IF NOT EXISTS Content (
        idContent    INTEGER       PRIMARY KEY
                                   UNIQUE,
        contentData  TEXT,
        contentTopic VARCHAR (255),
        groupId      INTEGER       REFERENCES [Groups] (idGroup) 
        );
        CREATE TABLE IF NOT EXISTS [Groups] (
        idGroup   INTEGER      PRIMARY KEY AUTOINCREMENT
                               UNIQUE,
        groupName VARCHAR (15) 
        );
        CREATE VIEW IF NOT EXISTS Main AS
        SELECT idContent,
               contentTopic,
              contentData,
              groupName
          FROM Content
               LEFT JOIN
              [Groups] ON idGroup = groupId;
        INSERT INTO Groups (groupName) VALUES ('Apps');
        INSERT INTO Groups (groupName) VALUES ('Commands');
        ";
      $db->exec($sql);
    }
  } else {
    $db = new PDO('sqlite:' . $filename);
    if (!$db) {
      echo "Error: Unable to connect to database.";
      exit();
    } 
  }
?>
