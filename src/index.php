<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>cuteKB</title>
  <link rel="stylesheet" href="lib/css/easymde.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/css/mdb.dark.min.css" />

  <script src="lib/js/easymde.min.js"></script>
  <script type="text/javascript" src="lib/js/mdb.min.js"></script>
  <script type="text/javascript" src="lib/js/jquery.min.js"></script>
  <script src="lib/js/functions.js"></script>

</head>

<body font-size-base="0.6rem">
  <!-- Modal Editor-->
  <div class="modal modal-xl fade modal-dialog-scrollable" data-mdb-backdrop="static" data-mdb-keyboard="false"
    id="editor" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="form-outline">
            <input type="text" id="topic" class="form-control rounded" />
            <label class="form-label" for="topic">Topic</label>
          </div>
          <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
        </div>
        <div id="modal-body" class="modal-body">
          <textarea id="editorarea" name="new"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">Close</button>
          <button id="btn-save" type="button" class="btn btn-primary" data-mdb-dismiss="modal">Save changes</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Delete-->
  <div class="modal fade modal-dialog-scrollable" data-mdb-backdrop="static" data-mdb-keyboard="false" id="delete"
    tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
          <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
        </div>
        <div id="delete-content" name="id" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">Close</button>
          <button id="btn-confirm" type="button" class="btn btn-danger" data-mdb-dismiss="modal">Delete</button>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <nav class="navbar sticky-top">
          <div class="container-fluid">
            <a class="navbar-brand">cuteKB <img src="img/ubs.svg" alt="" width="85" height="36"></a>
            <div class="btn-group" role="group">
              <button id="btn-new" type="button" class="btn btn-primary">New</button>
              <button id="btn-edit" type="button" class="btn btn-success disabled">Edit</button>
              <button id="btn-del" type="button" class="btn btn-danger disabled">Del</button>
            </div>
            <form class="d-flex input-group w-auto">
              <div class="input-group">
                <div class="form-outline">
                  <input id="search" type="text" id="form1" class="form-control rounded" />
                  <label class="form-label" for="form1">Search</label>
                </div>
                <button id="btn-refresh" type="button" class="btn btn-success">
                  <i class="fas fa-sync"></i>
                </button>
              </div>

            </form>
          </div>
        </nav>
      </div>
    </div>
    <div class="row">
      <div class="col-2">
        <table class="table table table-bordered table-hover table-sm">
          <thead>
            <tr>
              <th scope="col">Topic</th>
              <th scope="col">Group</th>
            </tr>
          </thead>
          <tbody id="sql_table">
          </tbody>
        </table>
      </div>
      <div id="content" class="col-8">
      </div>
      <div class="col-2 d-none d-lg-block">
        <div id="scrollspy" class="sticky-top" style="top: 80px;">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12"></div>
    </div>
  </div>
</body>

</html>